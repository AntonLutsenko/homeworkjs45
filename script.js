// используется для поиска и удаления символов
;
function createNewUser() {
    let firstName = prompt('Enter you name');
    let lastName = prompt('Enter you surname');
    let birthday = prompt("Enter your birthday date", "dd.mm.yyyy");
    let date = new Date;
    let birthdayMiliseconds = Date.parse(birthday.slice(6) + '.' + birthday.slice(3, 6) + birthday.slice(0, 2));

    let newUser = {
        firstName,
        lastName,
        birthday,
        birthdayMiliseconds,
        date,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        getAge() {
            return Math.floor((this.date - this.birthdayMiliseconds) / (1000 * 3600 * 24 * 365))
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + new Date (this.birthdayMiliseconds).getFullYear()
        },
    }
    return newUser;
}

let createUser = createNewUser();
let login = createUser.getLogin();
console.log(login);
let age = createUser.getAge();
console.log(age);
let pass = createUser.getPassword();
console.log(pass);
